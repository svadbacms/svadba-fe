import moment from 'moment';
import jwtDecode from 'jwt-decode';

export function formatTimestamp(timestamp) {
  const mmnt = moment.unix(timestamp / 1000);
  return mmnt.format('DD.MM YYYY');
}

export function getToken() {
  const token = localStorage.getItem('token');
  return token ? jwtDecode(token) : null;
}

export function setToken(token) {
  try {
    localStorage.setItem('token', token);
  } catch (error) {
    console.warn('Storage error', error);
  }
}

export function deleteToken() {
  localStorage.removeItem('token');
}

/**
 *
 * @param token token from redux store(or null). It's used when local storage is empty (because of disabled local storage)
 * @param method string, PUT, POST, GET, DELETE
 * @param url absolute URL
 * @param body body in JSON format
 * @param auth Default is true. If true, auth is required. If request should be public, auth should be false.
 * @returns {Promise.<TResult>|*} Promise return response in JSON format, or if auth is false, return full response object
 */
export const doFetch = (token, method, url, body, auth = true) => {
  const headers = new Headers();
  if (auth) {
    headers.append('x-access-token', localStorage.getItem('token') || token);
  }
  if (body) {
    headers.append('Content-Type', 'application/json');
  }
  const setup = {
    method: method,
    headers: headers,
    mode: 'cors',
    cache: 'default'
  };
  if (body) {
    setup.body = JSON.stringify(body);
  }
  return fetch(url, setup)
    .then(response => {
      // if not handle auth, send whole response next
      if (!auth) {
        return response;
      }
      if (response.status === 401 || response.status === 403) {
        alert('Už naďalej nie ste prihlásený. Otvorte nové okno, prihláste sa znovu a akciu zopakujte');
        throw response.status;
      }
      return response.json();
    });
};