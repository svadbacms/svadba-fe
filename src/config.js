/**
 * 6 colors of tasks (my own palette :))
 * @type {[*]}
 */
export const colors = [
  '#f4f4f4',
  '#f76882',
  '#a668f7',
  '#6887f7',
  '#68f773',
  '#eef768',
  '#f7b168'
];

export const baseUrl = process.env.NODE_ENV == 'development' ? 'http://localhost:3001' : 'http://159.65.205.222:3001';
