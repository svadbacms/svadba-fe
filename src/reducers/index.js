import {combineReducers} from 'redux';
import {tasks, tasksListing, tasksComments, credentials, isSorting} from './main';

const rootReducer = combineReducers({
  tasks,
  tasksListing,
  tasksComments,
  credentials,
  isSorting
});

export default rootReducer;