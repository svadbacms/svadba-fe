import * as actions from '../actions/actionTypes';
import {arrayMove} from 'react-sortable-hoc';

export const tasks = (state = {}, action) => {
  switch (action.type) {
    case actions.TASKS_LOADED:
      return mapArrayToObject(action.tasks.tasks);
    case actions.NEW_TASK_ADDED:
      return {[action.task._id]: action.task, ...state};
    case actions.TASK_MARKED_AS_DONE:
      return {...state, [action.task._id]: {...state[action.task._id], isDone: action.task.isDone}};
    case actions.TASK_EDITED:
      return {...state, [action.task._id]: action.task};
    case actions.TASK_DELETED:
      const newState2 = {...state};
      delete newState2[action.taskUuid];
      return newState2;
    default:
      return state;
  }
};

export const tasksListing = (state = [], action) => {
  switch (action.type) {
    case actions.TASKS_LOADED:
      return action.tasks.tasks.map(task => task._id);
    case actions.NEW_TASK_ADDED:
      return [action.task._id, ...state];
    case actions.TASK_DELETED:
      const newState = [...state];
      newState.splice(state.indexOf(action.taskUuid), 1);
      return newState;
    case actions.SORT_TASKS:
      return arrayMove(state, action.oldIndex, action.newIndex);
    default:
      return state;
  }
};

export const tasksComments = (state = {}, action) => {
  switch (action.type) {
    case actions.TASKS_LOADED:
      return mapArrayToObject(
        action.tasks.comments.map(task => ({...task, comments: mapArrayToObject(task.comments)})),
        'comments'
      );
    case actions.NEW_COMMENT_ADDED:
      return {...state, [action.comment.taskId]: {...state[action.comment.taskId] || {}, [action.comment._id]: action.comment}};
    case actions.COMMENT_EDITED:
      return {...state, [action.taskId]: {...state[action.taskId], [action.comment._id]: action.comment}};
    case actions.COMMENT_DELETED:
      const newState = {...state};
      delete newState[action.taskId][action.commentId];
      return newState;
    default:
      return state;
  }
};

/**
 *
 * @param array
 * @param valueField optional, if set, only "valueField" field will be as value in object, or whole item fro array otherwise
 * @param key optional, default is _id, which field should be key from each item
 * @returns {{key: {...valueField}} desired object
 */
function mapArrayToObject(array, valueField, key = '_id') {
  let object = {};
  array.map(item => (object = {...object, [item[key]]: item[valueField] || item}));
  return object;
}

export const credentials = (state = {isLogged: false, error: null, user: {}}, action) => {
  switch (action.type) {
    case actions.LOGIN_OK:
      // TODO is isLogged necessary?
      return {...state, isLogged: true, token: action.token};
    case actions.LOGINNING:
      return {...state, error: null};
    case actions.LOGIN_FAIL:
      return {...state, isLogged: false, error: action.error};
    case actions.NOTIFICATIONS_TOGGLED:
      return {...state, user: {...state.user, sendNotifications: action.user.sendNotifications}};
    case actions.GET_USER_INFO:
      return {...state, user: action.user};
    default:
      return state;
  }
};

export const isSorting = (state = false, action) => {
  switch (action.type) {
    case actions.SORTING_START:
      return true;
    case actions.SORTING_END:
      return false;
    default:
      return state;
  }
};

