import React, { Component } from 'react';
import DashboardContainer from './components/dashboard/DashboardContainer';
import LoginContainer from './components/login/LoginContainer';
import {getToken} from './utils/utils';

class App extends Component {
  render() {
    const token = getToken();
    const isTokenOk = token && token.exp > new Date().getTime() / 1000;
    return (
      <div>
        {/* if token is OK or redux store is OK(if storage is not enabled, e.g. in private mode in Safari) */}
        {isTokenOk || this.props.isLogged
          ? <DashboardContainer />
          : <LoginContainer />}
      </div>
    );
  }
}

export default App;
