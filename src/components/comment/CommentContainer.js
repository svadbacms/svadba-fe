import {connect} from 'react-redux';
import Comment from './Comment';
import {editComment, deleteComment} from '../../actions/main';
import {getToken} from '../../utils/utils';

const mapStateToProps = (state, ownProps) => {
  return {
    canEdit: getToken().role === ownProps.comment.userRole
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onSave(commentObject) {
      dispatch(editComment(ownProps.taskId, commentObject))
    },
    onDelete() {
      dispatch(deleteComment(ownProps.comment.taskId, ownProps.comment._id))
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Comment);
