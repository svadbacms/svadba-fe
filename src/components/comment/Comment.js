import React from 'react';
import PropTypes from 'prop-types';
import {ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import RichText from '../basic/RichText';
import NewComment from '../newComment/NewComment';
import DeleteDialog from '../basic/DeleteDialog';
import groom from './groom.png';
import bride from './bride.png';
import ImageView from '../basic/ImageView';
import {formatTimestamp} from "../../utils/utils";

export default class Comment extends React.Component {
  static propTypes = {
    comment: PropTypes.object,
    onSave: PropTypes.func,
    onDelete: PropTypes.func
  };

  state = {
    editMode: false,
    comment: this.props.comment,
    deleteDialog: false
  };

  handleOnEditMode = (comment) => {
    if (this.props.canEdit) {
      this.setState({editMode: true, comment});
    }
  };

  handleOnDiscard = () => {
    this.setState({editMode: false, comment: this.props.comment});
  };

  handleOnSave = (comment) => {
    this.props.onSave(comment);
    this.setState({editMode: false, comment});
  };

  handleOpenDeleteDialog = () => {
    this.setState({deleteDialog: true});
  };

  handleCloseDeleteDialog = () => {
    this.setState({deleteDialog: false});
  };

  render() {
    const {comment, canEdit, onDelete} = this.props;
    const text = <div>
      {this.state.comment.imageUrl && <ImageView src={this.state.comment.imageUrl} />}
      <RichText text={this.state.comment.text}/>
    </div>;
    return (
      <div>
        {this.state.editMode
          ? <NewComment onAdd={this.handleOnSave} onDiscard={this.handleOnDiscard} comment={comment} onDelete={this.handleOpenDeleteDialog}/>
          : <ListItem
          leftAvatar={<Avatar src={comment.userRole === 'GROOM' ? groom : bride} />}
          primaryText={text}
          secondaryText={formatTimestamp(comment.createdAt)}
          onClick={this.handleOnEditMode}
          disabled={!canEdit}
        />}
        <DeleteDialog
          text="Vymazať komentár?"
          visible={this.state.deleteDialog}
          onCloseDialog={this.handleCloseDeleteDialog}
          onDelete={onDelete} />
      </div>)
  };
};
