import React from 'react';
import css from './style.css';

const countDownDate = new Date("Nov 4, 2017 16:00:00").getTime();

export default class Countdown extends React.Component {
  state = {
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0
  };

  componentDidMount () {
    this.calculate();
    setInterval(() => {
      this.calculate();
    }, 1000);
  }

  calculate () {
    // Get todays date and time
    const now = new Date().getTime();

    // Find the distance between now an the count down date
    const distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    this.setState({
      days: Math.floor(distance / (1000 * 60 * 60 * 24)),
      hours: Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
      minutes: Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
      seconds: Math.floor((distance % (1000 * 60)) / 1000)
    });
  }

  render () {
    return (
      <div className={css.wrapper}>
        <span className={css.countdownText}>Do svadby ostáva</span>
        <div className={css.item}>
          <span className={css.number}>{this.state.days}</span>
          <div className={css.label}>dní</div>
        </div>
        <div className={css.item}>
          <span className={css.number}>{this.state.hours}</span>
          <div className={css.label}>hodín</div>
        </div>
        <div className={css.item}>
          <span className={css.number}>{this.state.minutes}</span>
          <div className={css.label}>minút</div>
        </div>
        <div className={css.item}>
          <span className={css.number}>{this.state.seconds}</span>
          <div className={css.label}>sekúnd</div>
        </div>
      </div>);
  }
}
