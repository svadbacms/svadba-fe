import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import CircularProgress from 'material-ui/CircularProgress';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';

import css from './style.css';
import bride from './bride.png';
import groom from './groom.png';
import bg from './bg.jpg';

const style = {
  circle: {
    position: 'absolute',
    right: 5,
    top: 6
  }
};

export default class Login extends React.Component {
  state = {
    accountType: 'BRIDE',
    errorText: '',
    password: '',
    isLogging: false
  };

  handleChangeAccountType = accountType => {
    this.setState({accountType});
    this.refs.passwordField.focus();
  };

  handleOnSubmit = () => {
    if (this.state.password.length > 0) {
      this.props.login(this.state.accountType, this.state.password);
      this.setState({isLogging: true});
    } else {
      this.setState({errorText: 'Zadajte heslo prosím'});
    }
  };

  handleOnPasswordChanged = (password) => {
    if (password.length > 0) {
      this.setState({errorText: ''});
    }
    this.setState({password});
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.loggingError) {
      this.setState({errorText: 'Nesprávne heslo', isLogging: false});
    }
  }

  handleKeypress = e => {
    if (e.charCode === 13) {
      this.handleOnSubmit()
    }
  };

  render() {
    return (
      <div className={css.wrapper} style={{backgroundImage: 'url(' + bg + ')'}}>
        <Card style={{marginTop: '15px', marginBottom: '15px'}}>
          <CardHeader textStyle={{margin: 0, padding: 0}} style={{flexGrow: 1}} title="Info pre hostí" subtitle="Harmonogram, Mapy, Ubytovanie" />
          <CardText>
            <div className={css.info}>
              <div className={css.header}>Harmonogram (4. November 2017)</div>
              <List>
                <ListItem primaryText="14:00 - 15:00 Guľáš" />
                <ListItem primaryText="15:00 - 16:00 Sobáš" />
                <ListItem primaryText="16:00 - 16:30 Gratulácie pri kostole" />
                <ListItem primaryText="16:30 - 20:00 Zábava" />
                <ListItem primaryText="20:00 - 21:00 Fotenie" />
                <ListItem primaryText="23:30 - 1:00 Družbovský + Redovy" />
              </List>
              <div className={css.header}>Sála a kostol</div>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3524.3387284259775!2d21.26354313451384!3d49.17327725694933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473e8f47c660f8b1%3A0xa6f8b9b6cc718f7c!2sr%C3%ADmskokatol%C3%ADcky+kostol+sv.+Michala+archanjela!5e0!3m2!1ssk!2ssk!4v1509367212485" width="300" height="400" frameBorder="0" allowFullScreen></iframe>
              <div>Sú hneď pri sebe, zaparkujte kdekoľvek<br /><br/></div>
              <div className={css.header}>Taxi do Raslavíc</div>
                <a href="http://www.penzion-raslavican.sk/taxi-sluzba">http://www.penzion-raslavican.sk/taxi-sluzba</a>
                <a href="tel:0907091343">0907 091 343</a>
                <a href="tel:0907414089">0907 414 089</a>
                Cena: 0,60 €/km<br/><br/>
              <div className={css.header}>Penzión Raslavičan</div>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2609.621539110905!2d21.31216945117969!3d49.15080968792501!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473e8fb09007a2d9%3A0xa399d078f66974a1!2sPenzion+Raslavi%C4%8Dan!5e0!3m2!1ssk!2ssk!4v1509366400037" width="300" height="400" frameBorder="0" allowFullScreen></iframe>
              <div>Kontakt:<br/>
                <a href="http://www.penzion-raslavican.sk/kontakt">http://www.penzion-raslavican.sk/kontakt</a><br/>
                <a href="tel:0905519584">0905 519 584</a><br/>
                Hlavná 236 Raslavice<br/><br/>
              </div>
            </div>
          </CardText>
        </Card>
        <div className={css.window}>
          <div className={css.header}>Chcem sa prihlásiť ako:</div>
          <div className={css.buttons}>
            <RaisedButton
              label="Nevesta"
              labelPosition="before"
              primary={this.state.accountType === 'BRIDE'}
              icon={<img src={bride} className={css.icon}/>}
              onTouchTap={() => this.handleChangeAccountType('BRIDE')}
            />
            <RaisedButton
              label="Ženích"
              primary={this.state.accountType === 'GROOM'}
              icon={<img src={groom} className={css.icon} />}
              onTouchTap={() => this.handleChangeAccountType('GROOM')}
            />
          </div>
          <TextField
            hintText={'Zadajte heslo ' + (this.state.accountType === 'BRIDE' ? 'nevesty' : 'ženícha')}
            floatingLabelText="Heslo"
            type="password"
            fullWidth={true}
            errorText={this.state.errorText}
            ref="passwordField"
            onChange={(e, value) => this.handleOnPasswordChanged(value)}
            onKeyPress={this.handleKeypress}
            autoFocus
          />
          <RaisedButton label="Prihlásiť" fullWidth={true} onTouchTap={this.handleOnSubmit} disabled={this.state.isLogging}>
            {this.state.isLogging && <CircularProgress size={24} style={style.circle} thickness={2} color="black" />}
          </RaisedButton>
        </div>
      </div>
    )
  }
}
