import {connect} from 'react-redux';
import Login from './Login';
import {login} from '../../actions/main';

const mapStateToProps = (state, ownProps) => {
  return {
    loggingError: state.credentials.error > 200
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    login(accountType, password) {
      dispatch(login(accountType, password));
    }
  }
};

const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

export default LoginContainer;
