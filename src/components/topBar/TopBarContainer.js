import {connect} from 'react-redux';
import TopBar from './TopBar';
import {toggleNotifications} from '../../actions/main';

const mapStateToProps = (state, ownProps) => {
    return {
      sendNotifications: state.credentials.user.sendNotifications || false,
      name: state.credentials.user.name || ''
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onToggleNotifications(checked) {
      dispatch(toggleNotifications(checked));
    }
  }
};

const TopBarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TopBar);

export default TopBarContainer;
