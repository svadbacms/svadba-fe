import React from 'react';
import PropTypes from 'prop-types';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import IconMenu from 'material-ui/IconMenu';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import MenuItem from 'material-ui/MenuItem';
import {yellow500, grey500} from 'material-ui/styles/colors';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';

import css from './style.css';
import Countdown from '../countdown/Countdown';
import {deleteToken} from '../../utils/utils';

const TopBar = ({onToggleNotifications, sendNotifications, name}) => (
  <Toolbar style={{backgroundColor: yellow500}}>
    <ToolbarGroup>
      <div className={css.title}>
        <ToolbarTitle text="Naša svadba" style={{fontFamily: 'Monsieur La Doulaise', fontSize: '32px', color: grey500}}/>
      </div>
    </ToolbarGroup>
    <ToolbarGroup>
      <Countdown/>
      <ToolbarSeparator />
      <IconMenu
        iconButtonElement={
          <FlatButton
            label={name}
            labelPosition="before"
            icon={<NavigationExpandMoreIcon />}
          />
        }
      >
        <Checkbox label="Notifikácie emailom" onCheck={(e, value) => onToggleNotifications(value)} checked={sendNotifications} iconStyle={{marginLeft: '8px'}}/>
        <MenuItem primaryText="Odhlásiť sa" onTouchTap={() => {deleteToken(); location.reload()}} />
      </IconMenu>
    </ToolbarGroup>
  </Toolbar>
);

TopBar.propTypes = {
  onToggleNotifications: PropTypes.func.isRequired,
  sendNotifications: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired
};

export default TopBar;
