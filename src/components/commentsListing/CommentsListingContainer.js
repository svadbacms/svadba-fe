import {connect} from 'react-redux';
import CommentsListing from './CommentsListing';

const mapStateToProps = (state, ownProps) => {
  const comments = state.tasksComments[ownProps.taskId] || {};
  return {
    comments: Object.keys(comments).map(id => comments[id])
  };
};

export default connect(
  mapStateToProps
)(CommentsListing);
