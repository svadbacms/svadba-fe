import React from 'react';
import PropTypes from 'prop-types';
// import css from './style.css';
import {List} from 'material-ui/List';
import CommentContainer from '../comment/CommentContainer';

const CommentsListing = ({taskId, comments}) => (
  <List>
    {comments.map(comment => <CommentContainer key={comment._id} taskId={taskId} comment={comment} />)}
    {comments.length === 0 && <span>Žiadne komentáre</span>}
  </List>
);

CommentsListing.propTypes = {
  taskId: PropTypes.string,
  comments: PropTypes.array
};

export default CommentsListing;