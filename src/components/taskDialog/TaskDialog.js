import React from 'react';
import PropTypes from 'prop-types';
import css from './style.css';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import {formatTimestamp} from '../../utils/utils';
import Checkbox from 'material-ui/Checkbox';
import ColorPicker from '../../components/basic/ColorPicker';

export default class TaskDialog extends React.Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    onSave: PropTypes.func,
    onClose: PropTypes.func,
    isOpen: PropTypes.bool
  };

  state = {
    ...this.props.task,
    doUntil: this.props.task ? this.props.task.doUntil : new Date().getTime(),
    color: this.props.task ? this.props.task.color : 0
  };

  handleTitle = (title) => {
    this.setState({title});
  };

  handleDoUntil = (timestamp) => {
    this.setState({doUntil: timestamp});
  };

  handleIsDone = (isDone) => {
    this.setState({isDone});
  };

  handleDescription = (description) => {
    this.setState({description});
  };

  handleColorChanged = (i, color) => {
    this.setState({color: i});
  };

  render () {
    const actions = [
      <FlatButton
        label="Zrušiť"
        primary={false}
        onTouchTap={this.props.onClose}
      />,
      <FlatButton
        label="Uložiť"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => this.props.onSave(this.state)}
      />,
    ];
    return (
      <div>
        <Dialog
          title={this.props.title}
          actions={actions}
          modal={true}
          open={this.props.isOpen}
          onRequestClose={this.props.onClose}
        >
          <ColorPicker onSelect={this.handleColorChanged} activeColorPosition={this.state.color} />
          <TextField
            defaultValue={this.state.title}
            floatingLabelText="Nadpis"
            fullWidth={true}
            onChange={(e, value) =>this.handleTitle(value)}
          />
          <div className={css.inline}>
            <DatePicker
              disabled={this.state.isDone}
              style={{flex: 1}}
              hintText="Splniť do"
              container="inline"
              autoOk={true}
              defaultDate={new Date(this.state.doUntil)}
              formatDate={date => formatTimestamp(date.getTime())}
              onChange={(e, date) => this.handleDoUntil(date.getTime())}
            />
            <div className={css.isDone}>
            <Checkbox label="alebo je úloha splnená" labelPosition='left' onCheck={(e, i) => this.handleIsDone(i)}/>
            </div>
          </div>
          <TextField
            defaultValue={this.state.description}
            floatingLabelText="Popis"
            multiLine={true}
            rows={4}
            rowsMax={4}
            fullWidth={true}
            onChange={(e, value) => this.handleDescription(value)}
          />
        </Dialog>
      </div>
    )

  }
};
