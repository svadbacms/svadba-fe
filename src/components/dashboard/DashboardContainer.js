import {connect} from 'react-redux';
import Dashboard from './Dashboard';
import {getUserInfo} from '../../actions/main';

const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getUserInfo() {
      dispatch(getUserInfo());
    }
  }
};

const DashboardContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

export default DashboardContainer;
