import React, { Component } from 'react';
import TasksListingContainer from '../tasksListing/TasksListingContainer';
import TopBarContainer from '../topBar/TopBarContainer';
import EmojiHelper from '../basic/EmojiHelper';

class Dashboard extends Component {

  componentDidMount() {
    this.props.getUserInfo();
  }

  render() {
    return <div>
        <TopBarContainer />
        <EmojiHelper />
        <TasksListingContainer />
      </div>
    ;
  }
}

export default Dashboard;
