import React from 'react';
import css from './style.css';
import TaskDialog from '../taskDialog/TaskDialog';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

const style = {
  position: 'fixed',
  right: 20,
  bottom: 20,
  zIndex: 9
};

/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */
export default class NewTask extends React.Component {
  state = {
    open: false
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleSave = (task) => {
    this.props.saveNewTask(task);
    // TODO Handle in reducer because of possible errors in fetch()
    this.setState({open: false});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    return (
      <div className={css.wrapper}>
        <FloatingActionButton style={style} onTouchTap={this.handleOpen}>
          <ContentAdd />
        </FloatingActionButton>
        {this.state.open ? <TaskDialog
          title="Nová úloha"
          task={this.state.task}
          onClose={this.handleClose}
          onSave={this.handleSave}
          isOpen
        /> : ''}
      </div>
    );
  }
}