import {connect} from 'react-redux';
import NewTask from './NewTask';
import {addNewTask} from '../../actions/main';

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveNewTask(taskObject) {
      dispatch(addNewTask(taskObject))
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewTask);
