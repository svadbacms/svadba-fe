import React from 'react';
import PropTypes from 'prop-types';
import css from './style.css';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import AddPhoto from 'material-ui/svg-icons/image/add-a-photo';
// import FileUpload from 'material-ui/svg-icons/file/file-upload';
import ImageView from '../basic/ImageView';

export default class NewComment extends React.Component {
  static defaultProps = {
    comment: {},
    onDiscard: () => {}
  };

  static propTypes = {
    onAdd: PropTypes.func,
    onDiscard: PropTypes.func,
    onDelete: PropTypes.func,
    comment: PropTypes.object
  };

  // handle both new comment and edit existing comment
  state = {
    comment: {
      ...this.props.comment,
      text: this.props.comment.text || '',
      imageUrl: this.props.comment.imageUrl || ''
    },
    showImageUrlInput: this.props.comment.imageUrl ? true : false
  };

  componentDidMount() {
    // focus to text field only if editing comment
    if (this.props.comment._id) {
      this.refs.commentTextInput.focus();
    }
  }

  handleOnChange = (text) => {
    this.setState({comment: {...this.state.comment, text}});
  };

  handleOnSave = () => {
    this.setState({comment: {...this.state.comment, text: '', imageUrl: ''}, showImageUrlInput: false});
    this.props.onAdd(this.state.comment);
  };

  handleOnDiscard = () => {
    this.setState({comment: {...this.state.comment, text: '', imageUrl: ''}, showImageUrlInput: false});
    this.props.onDiscard();
  };

  handleOnDelete = () => {
    this.props.onDelete(this.state.comment);
  };

  handleAddPhotoUrl = () => {
    this.setState({showImageUrlInput: !this.state.showImageUrlInput});
  };

  handleOnPhotoUrlChange = (imageUrl) => {
    this.setState({comment: {...this.state.comment, imageUrl}, noErrors: true});
  };

  render() {
    return (
      <div className={css.wrapper}>
        {this.state.comment.imageUrl && <ImageView src={this.state.comment.imageUrl} />}
        <TextField
          floatingLabelText={this.state.comment._id ? 'Upraviť komentár' : 'Nový komentár'}
          multiLine={true}
          rows={1}
          rowsMax={4}
          fullWidth={true}
          onChange={(e, value) => this.handleOnChange(value)}
          value={this.state.comment.text}
          ref="commentTextInput"
        />
        <div className={css.actionsWrapper}>
          <div className={css.photoInput}>
            {this.state.showImageUrlInput && <TextField
              hintText="URL obrázka"
              rows={1}
              onChange={(e, value) => this.handleOnPhotoUrlChange(value)}
              type="url"
              value={this.state.comment.imageUrl}
              style={{flexGrow: 1, width: 'initial'}}
            />}
            <IconButton onTouchTap={this.handleAddPhotoUrl}>
              <AddPhoto />
            </IconButton>
          </div>
          <div className={css.actions}>
            {/*<IconButton>*/}
              {/*<FileUpload />*/}
            {/*</IconButton>*/}
            {this.state.comment._id &&
            <FlatButton
              label="Odstrániť"
              primary={true}
              onTouchTap={this.handleOnDelete}
            />}
            {(this.state.comment.text.length > 0 || this.state.comment.imageUrl) &&
              <div className={css.buttons}>
                <FlatButton
                  label="Zrušiť"
                  primary={true}
                  onTouchTap={this.handleOnDiscard}
                />
                <FlatButton
                  label="Uložiť"
                  primary={true}
                  onTouchTap={this.handleOnSave}
                />
              </div>}
          </div>
        </div>
      </div>
    );
  }
}