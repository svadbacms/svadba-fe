import {connect} from 'react-redux';
import NewComment from './NewComment';
import {addNewComment} from '../../actions/main';

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onAdd(commentObject) {
      dispatch(addNewComment(ownProps.taskId, commentObject.text, commentObject.imageUrl))
    },
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewComment);
