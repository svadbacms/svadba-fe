import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

const DeleteDialog = ({text, visible = false, onCloseDialog, onDelete}) => {
  const actions = [
    <FlatButton
      label="Zrušiť"
      primary={true}
      onTouchTap={onCloseDialog}
    />,
    <FlatButton
      label="Odstrániť"
      primary={true}
      onTouchTap={onDelete}
    />,
  ];
  return visible
    ? <Dialog
      actions={actions}
      modal={false}
      open={visible}
      onRequestClose={onCloseDialog}
      >
        {text}
      </Dialog>
    : <span />

};

DeleteDialog.propTypes = {
  text: PropTypes.string,
  visible: PropTypes.bool,
  onCloseDialog: PropTypes.func,
  onDelete: PropTypes.func
};

export default DeleteDialog;