import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import placeholder from './placeholder.jpg';

const handleOnErrorImage = event => {
  event.target.src = placeholder;
};

const style = {
  margin: '0 auto 10px'
};

const ImageView = ({src}) => (
  <Paper zDepth={2} style={style}>
    <img src={src} width="100%" onError={handleOnErrorImage} alt={src} />
  </Paper>);

ImageView.propTypes = {
  src: PropTypes.string
};

export default ImageView;