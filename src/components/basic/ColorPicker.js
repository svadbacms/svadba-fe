import React from 'react';
import PropTypes from 'prop-types';
import css from './colorPicker.css';
import {colors as defaultColors} from '../../config';

const ColorPicker = ({colors = defaultColors, onSelect, activeColorPosition = 0}) => (
  <div className={css.wrapper}>
    {colors.map((color, i) => <div
      key={color}
      className={css.color + ' ' + (i === activeColorPosition ? css.activeColor: '')}
      style={{backgroundColor: color}}
      onClick={() => onSelect(i, color)}
    ></div>)}
  </div>
);

ColorPicker.propTypes = {
  colors: PropTypes.array,
  onSelect: PropTypes.func,
  activeColorPosition: PropTypes.number
};

export default ColorPicker;
