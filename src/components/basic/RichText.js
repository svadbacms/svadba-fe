import React from 'react';
import PropTypes from 'prop-types';
import Linkify from 'react-linkify';
import ReactEmoji from 'react-emoji';
import {blueGrey600} from 'material-ui/styles/colors';

/**
 * Handle propagation click event - block it to all parent, because this is pure URL link
 * @param e
 */
const handleOnClick = e => {
  e.stopPropagation();
  e.nativeEvent.stopImmediatePropagation();
};

const style = {
  color: blueGrey600
};

const emojiProps = {
  emojiType: 'emojione',
  attributes: {width: '18px', height: '18px'}
};

const RichText = ({text}) => (
  <div style={{lineHeight: '20px', wordWrap: 'break-word', whiteSpace: 'pre-wrap'}}>
    <Linkify properties={{target: '_blank', onClick: handleOnClick, style}}>{ReactEmoji.emojify(text, emojiProps)}</Linkify>
  </div>
);

RichText.propTypes = {
  text: PropTypes.string
};

export default RichText;