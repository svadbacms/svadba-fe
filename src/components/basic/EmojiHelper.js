import React from 'react';

const EmojiHelper = ({text}) => (
  <div style={{fontSize: '12px', marginTop: '10px', textAlign: 'center'}}>
    <div>Nevieš akí smajlíci existujú? <a href="https://emoji.codes" target="_blank">Inšpiruj sa tu</a></div>
  </div>
);

export default EmojiHelper;