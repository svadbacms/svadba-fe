import React from 'react';
import PropTypes from 'prop-types';
import {Icon} from 'react-fa'
import css from './circleIcon.css';

const CircleIcon = ({style}) => (
  <Icon className={css.icon} name="circle-thin" size="2x" style={style} />
);

CircleIcon.propTypes = {
  style: PropTypes.any
};

export default CircleIcon;