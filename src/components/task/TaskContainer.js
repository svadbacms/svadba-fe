import {connect} from 'react-redux';
import Task from './Task';
import {markTaskAsDone, editTask, deleteTask} from '../../actions/main';
import {SortableElement} from 'react-sortable-hoc';

const mapStateToProps = (state, ownProps) => {
  const comments = state.tasksComments[ownProps.taskUuid];
  return {
    task: state.tasks[ownProps.taskUuid],
    commentsCount: comments ? Object.keys(comments).length : 0,
    isSorting: state.isSorting
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    markAsDone(isDone) {
      dispatch(markTaskAsDone(ownProps.taskUuid, isDone));
    },
    editTask(task) {
      dispatch(editTask(task));
    },
    deleteTask() {
      dispatch(deleteTask(ownProps.taskUuid))
    }
  }
};

const TaskContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SortableElement(Task));

export default TaskContainer;
