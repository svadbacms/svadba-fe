import React from 'react';
import PropTypes from 'prop-types';
import css from './style.css';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import {formatTimestamp} from '../../utils/utils';
import TaskDialog from '../taskDialog/TaskDialog';
import CheckCircle from 'material-ui/svg-icons/action/check-circle';
import Checkbox from 'material-ui/Checkbox';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import CommentsListingContainer from '../commentsListing/CommentsListingContainer';
import NewCommentContainer from '../newComment/NewCommentContainer';
import RichText from '../basic/RichText';
import DeleteDialog from '../basic/DeleteDialog';
import {SortableHandle} from 'react-sortable-hoc';
import IconDragHandle from 'material-ui/svg-icons/editor/drag-handle';
import {colors} from '../../config';

const DragHandle = SortableHandle(() => <IconButton iconStyle={{fill: '#d2d2d2'}}><IconDragHandle/></IconButton>);

export default class Task extends React.Component {

  static propTypes = {
    task: PropTypes.object.isRequired,
    markAsDone: PropTypes.func.isRequired,
    editTask: PropTypes.func.isRequired,
    deleteTask: PropTypes.func.isRequired,
    commentsCount: PropTypes.number.isRequired,
    isSorting: PropTypes.bool.isRequired
  };

  state = {
    showComments: false,
    open: false,
    deleteDialog: false
  };

  handleShowComments = () => {
    this.setState({showComments: !this.state.showComments});
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleSave = (task) => {
    this.props.editTask(task);
    // TODO Handle in reducer because of possible errors in fetch()
    this.setState({open: false});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleOpenDeleteDialog = () => {
    this.setState({deleteDialog: true});
  };

  handleCloseDeleteDialog = () => {
    this.setState({deleteDialog: false});
  };

  render () {
    const {task, markAsDone, deleteTask, commentsCount, isSorting} = this.props;
    const taskDialog = this.state.open && <TaskDialog
      title="Upraviť úlohu"
      task={this.props.task}
      onClose={this.handleClose}
      onSave={this.handleSave}
      isOpen
    />;
    return (
      <Card style={{width: '100%', marginTop: '15px', textDecoration: task.isDone ? 'line-through' : 'none', position: 'relative'}}>
        <div className={css.colorStrip} style={{backgroundColor: colors[task.color]}}></div>
        <div className={css.header}>
          <div>
            <Checkbox
              checkedIcon={<CheckCircle/>}
              uncheckedIcon={<CheckCircle style={{fill: '#d2d2d2'}}/>}
              iconStyle={{width: '32px', height: '32px', marginRight: 0}}
              checked={task.isDone}
              onCheck={(e, value) => markAsDone(value)}
            />
          </div>
          <CardHeader textStyle={{margin: 0, padding: 0}} style={{flexGrow: 1}} title={<RichText text={task.title}/>} subtitle={'Splniť do ' + formatTimestamp(task.doUntil) + ' (vytvorené ' + formatTimestamp(task.createdAt) + ')'} />
          <DragHandle />
          <IconMenu
            iconButtonElement={<IconButton iconStyle={{fill: '#8e8e8e'}} ><MoreVertIcon /></IconButton>}
            anchorOrigin={{horizontal: 'left', vertical: 'top'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
          >
            <MenuItem primaryText="Upraviť" onTouchTap={this.handleOpen}/>
            <MenuItem primaryText="Odstrániť" onTouchTap={this.handleOpenDeleteDialog} />
          </IconMenu>
        </div>
        {!isSorting && task.description && <CardText>
          <RichText text={task.description}/>
        </CardText>}
        {!isSorting && task.comments && <FlatButton label={(this.state.showComments ? 'Menej' : 'Viac') + ' (' + commentsCount + ')'}
                    fullWidth={true} onClick={this.handleShowComments}/>
        }
        {!isSorting && this.state.showComments && <CardText style={{paddingTop: 0}}>
          <CommentsListingContainer taskId={task._id} />
          <NewCommentContainer taskId={task._id}/>
        </CardText>}
        {taskDialog}
        <DeleteDialog
          text="Vymazať úlohu?"
          visible={this.state.deleteDialog}
          onCloseDialog={this.handleCloseDeleteDialog}
          onDelete={deleteTask} />
      </Card>
    );
  }
}
