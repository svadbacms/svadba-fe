import React from 'react';
import PropTypes from 'prop-types';
import SortableList from './SortableList';

export default class TasksListing extends React.Component {
  static propTypes = {
    tasks: PropTypes.array.isRequired
  };

  componentDidMount() {
    this.props.loadTasks();
  }

  handleOnSortEnd = ({oldIndex, newIndex}) => {
    this.props.onSortEnd(this.props.tasks[oldIndex], oldIndex, newIndex, this.props.tasks.length);
  };

  handleSortStart = () => {
    this.props.onSortStart();
  };

  render () {
    return (
        <SortableList
          onSortStart={this.handleSortStart}
          onSortEnd={this.handleOnSortEnd}
          tasks={this.props.tasks}
          useDragHandle={true}
          useWindowAsScrollContainer={true}
          lockAxis="y"
        />
    )
  }
}
