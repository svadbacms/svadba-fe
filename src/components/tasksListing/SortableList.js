import React from 'react';
import {SortableContainer} from 'react-sortable-hoc';
import css from './style.css';
import NewTaskContainer from '../newTask/NewTaskContainer';
import TaskContainer from '../task/TaskContainer';

const SortableList = SortableContainer(({tasks}) => {
  return (
    <div className={css.wrapper}>
      <NewTaskContainer />
      {tasks.map((taskUuid, i) => <TaskContainer key={taskUuid} taskUuid={taskUuid} index={i} />)}
    </div>
  );
});

export default SortableList;