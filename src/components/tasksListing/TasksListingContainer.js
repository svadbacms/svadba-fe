import {connect} from 'react-redux';
import TasksListing from './TasksListing';
import {loadTasks, sortTasks, sortingEnd} from '../../actions/main';

const mapStateToProps = (state, ownProps) => {
  return {
    tasks: state.tasksListing
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loadTasks() {
      dispatch(loadTasks());
    },
    onSortEnd(taskUuid, oldIndex, newIndex, totalTasksCount) {
      if (oldIndex !== newIndex) {
        dispatch(sortTasks(taskUuid, oldIndex, newIndex, totalTasksCount));
      }
      dispatch(sortingEnd());
    },
    onSortStart() {
      // TODO use it or not?? It's for make height of tasks at minimum while reordering
      // dispatch(sortingStart());
    }
  }
};

const ListingContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TasksListing);

export default ListingContainer;
