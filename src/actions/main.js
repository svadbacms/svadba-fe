import * as actions from './actionTypes';
import {getToken, setToken, deleteToken, doFetch} from '../utils/utils';
import {baseUrl} from '../config';

// TODO split to particular files by logic

export const newTaskAdded = (task) => ({
  type: actions.NEW_TASK_ADDED,
  task
});

export const taskEdited = (task) => ({
  type: actions.TASK_EDITED,
  task
});

export const taskMarkedAsDone = (task) => ({
  type: actions.TASK_MARKED_AS_DONE,
  task
});

export const tasksLoaded = (tasks) => ({
  type: actions.TASKS_LOADED,
  tasks
});

export const taskDeleted = (taskUuid) => ({
  type: actions.TASK_DELETED,
  taskUuid
});

export const newCommentAdded = (comment) => ({
  type: actions.NEW_COMMENT_ADDED,
  comment
});

export const commentEdited = (taskId, comment) => ({
  type: actions.COMMENT_EDITED,
  taskId,
  comment
});

export const commentDeleted = (taskId, commentId) => ({
  type: actions.COMMENT_DELETED,
  taskId,
  commentId
});

export const loadTasks = () => {
  return function (dispatch, getState) {
     doFetch(getState().credentials.token, 'GET', baseUrl + '/tasks')
      .then(tasks => dispatch(tasksLoaded(tasks)))
  }
};

export const addNewTask = task => {
  return function (dispatch, getState) {
    doFetch(getState().credentials.token, 'PUT', baseUrl + '/task', task)
      .then(responseTask => dispatch(newTaskAdded(responseTask)))
  }
};

export const editTask = task => {
  return function (dispatch, getState) {
    doFetch(getState().credentials.token, 'POST', baseUrl + '/task', task)
      .then(responseTask => dispatch(taskEdited(responseTask)));
  }
};

export const markTaskAsDone = (taskUuid, isDone) => {
  return function (dispatch, getState) {
    doFetch(getState().credentials.token, 'POST', baseUrl + '/task', {_id: taskUuid, isDone})
      .then(responseTask => dispatch(taskMarkedAsDone(responseTask)))
  }
};

export const deleteTask = taskUuid => {
  return function (dispatch, getState) {
    doFetch(getState().credentials.token, 'DELETE', baseUrl + '/task', {_id: taskUuid})
      .then(responseTask => dispatch(taskDeleted(responseTask._id)))
  }
};

export const addNewComment = (taskId, text, imageUrl) => {
  return function (dispatch, getState) {
    const commentObject = {
      taskId,
      text,
      imageUrl,
      userRole: getToken().role
    };
    doFetch(getState().credentials.token, 'PUT', baseUrl + '/comment', commentObject)
      .then(responseComment => dispatch(newCommentAdded(responseComment)))
  }
};

export const editComment = (taskId, comment) => {
  return function (dispatch, getState) {
    const commentObject = {
      taskId,
      ...comment
    };
    doFetch(getState().credentials.token, 'POST', baseUrl + '/comment', commentObject)
      .then(responseComment => dispatch(commentEdited(taskId, responseComment)))
  }
};

export const deleteComment = (taskId, commentId) => {
  return function (dispatch, getState) {
    const commentObject = {_id: commentId};
    doFetch(getState().credentials.token, 'DELETE', baseUrl + '/comment', commentObject)
      .then(responseComment => dispatch(commentDeleted(taskId, commentId)))
  }
};

/**
 * Notify app that user is logged. It should NOT handle security, it's just for UI decisions
 */
export const loginOK = token => ({
  type: actions.LOGIN_OK,
  token
});

export const loginning = () => ({
  type: actions.LOGINNING
});

export const loginFail = error => ({
  type: actions.LOGIN_FAIL,
  error
});

export const login = (accountType, password) => {
  return function (dispatch, getState) {
    dispatch(loginning());
    const object = {
      name: accountType === 'GROOM' ? 'zenich' : 'nevesta',
      password
    };
    doFetch(null, 'POST', baseUrl + '/users/authenticate', object, false)
      .then(response => {
        // TODO do it in better way (somehow)
        if (response.status > 201) {
          throw response.status;
        }
        return response.json();
      })
      .then(response => {
        // next 2 lines order matters - setToken() is blocking operation so we can assume data are stored when call dispatch()
        setToken(response.token);
        // store token also to redux due private mode in Safari. It's used when local storage is not available, but
        // then when user refresh page, token is lost and user has to login again (but in private mode it's OK)
        dispatch(loginOK(response.token));
      })
      .catch(error => {
        deleteToken();
        dispatch(loginFail(error));
      })
  }
};

export const sortTasks = (taskUuid, oldIndex, newIndex, totalTasksCount) => {
  return function (dispatch, getState) {
    // Happy path update
    dispatch({type: actions.SORT_TASKS, oldIndex, newIndex});

    const commentObject = {
      _id: taskUuid,
      newIndex: totalTasksCount - newIndex - 1, // we have to revert index because on FE it is reverted (due DND lib)
      oldIndex: totalTasksCount - oldIndex - 1  // we have to revert index because on FE it is reverted (due DND lib)
    };
    doFetch(getState().credentials.token, 'POST', baseUrl + '/task/updateOrder', commentObject);
  }
};

export const sortingStart = () => ({
  type: actions.SORTING_START
});

export const sortingEnd = () => ({
  type: actions.SORTING_END
});

export const notificationsToggled = user => ({
  type: actions.NOTIFICATIONS_TOGGLED,
  user
});

export const toggleNotifications = (sendNotifications) => {
  return function (dispatch, getState) {
    doFetch(getState().credentials.token, 'POST', baseUrl + '/users/toggleNotifications', {sendNotifications})
      .then(response => dispatch(notificationsToggled(response)));
  }
};

export const userLogged = user => ({
  type: actions.GET_USER_INFO,
  user
});

export const getUserInfo = () => {
  return function (dispatch, getState) {
    doFetch(getState().credentials.token, 'GET', baseUrl + '/users/info')
      .then(response => dispatch(userLogged(response)));
  }
};
